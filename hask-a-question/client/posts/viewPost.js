Session.setDefault('visible',false)



Template.viewPost.helpers({
   'post' : function(){
   	 var appId = FlowRouter.getParam("post_id");
     console.log( "MESSAGE" , appId);
     //Meteor.subscribe('viewPost', appId);
     console.log(Posts.collection.findOne(appId))
     return Posts.collection.findOne(appId);
   },
   
   'comments': () => {
    	//Meteor.subscribe('allPosts')
		var appId = FlowRouter.getParam("post_id");
		console.log( "MESSAGE" , appId);
	    var comments = Comments.find({post: appId}, { sort: { votes: 1 }});
	    console.log(comments);
	    return Comments.find({post: appId}, { sort: { votes: -1 }});
    },
	
	showForm : function(){
		var show = Session.get('visible');
		if( show === true){
			return true;
		}else{
			return false;
		}
	}
});

Template.viewPost.events({
	'click #addComment': function(){
		Session.set('visible',true)
	},
	'click #removeComment': function(){
		Session.set('visible',false)
	},
	'click .vote-up': function(){
		var post_id = FlowRouter.getParam("post_id");
      var vote_css_down = document.getElementById("vote-icon-qdow");
      vote_css_down.style.fontWeight = "normal";
      vote_css_down.style.color = "black";
      vote_css_down.style.textShadow = "0px 0px 0px black";
		console.log(post_id);
      Meteor.call('questionUpvote', post_id);
      var vote_css_up = document.getElementById("vote-icon-qup");
      vote_css_up.style.fontWeight = "bold";
      vote_css_up.style.color = "#42235A";
      vote_css_up.style.textShadow = "0px 0px 1px #42235A";
   },
	'click .vote-down': function() {
      var vote_css_up = document.getElementById("vote-icon-qup");
      vote_css_up.style.fontWeight = "normal";
      vote_css_up.style.color = "black";
      vote_css_up.style.textShadow = "0px 0px 0px black";
		var post_id = FlowRouter.getParam("post_id");
      var vote_css_down = document.getElementById("vote-icon-qdow");
      vote_css_down.style.fontWeight = "bold";
      vote_css_down.style.color = "#42235A";
      vote_css_down.textShadow = "0px 0px 1px #42235A";
      Meteor.call('questionDownvote', post_id);

	},
//	'submit form': function(event, template){
	'click #submit' : function(event,template){	
		Session.set('visible',false);
		
		if (! Meteor.userId()) {
			throw new Meteor.Error("not-authorized");
		}
      var appId = FlowRouter.getParam("post_id");
      var smarkdown = Session.get('editor-markdown');
      var shtml = Session.get('editor-html');
      var sword_count = Session.get('editor-word-count');
      var listName = $('[name=newcomment]').val();
      Meteor.call('submitNewComment', appId, smarkdown, shtml, sword_count, listName);
      $('[name=newcomment]').val('');

    }
});

/*
window.onload = function (e) {
   session.set('visible', false);
}
*/

//Template.commentSection.destroyed = function() {
//	Session.set('visible',false)
//}
