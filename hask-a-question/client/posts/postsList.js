/*
Template.postsList.onCreated(() => {
	this.subscribe('allPosts');
});
*/


if (Meteor.isClient){
   var subscription = Meteor.subscribeWithPagination("allPosts", 7);
   Template.postsList.helpers({
       'posts': () => {
          return Posts.sorted(subscription.loaded());
          //console.log(posts);
          //return posts;
       },
       'loading': () => {
          return !subscription.ready();
       },
       'hasMore': () => {
          return Posts.sorted(subscription.loaded()).count() == subscription.limit();
       }
   });

   Template.postsList.events({
      'click #addPost': (event, Template) => {
            FlowRouter.go('/addPost');
      },
      'click #loadMore': (event, Template) => {
         event.preventDefault();
         subscription.loadNextPage();
      }

   });
}

