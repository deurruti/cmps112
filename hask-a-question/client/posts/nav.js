//This file will contain any javascript helpers and events we need. 


Template.navBar.helpers({
    'authInProcess' : function (){
      return Meteor.loggingIn();
    },
    'canShow': function(){
       return !!Meteor.user();
    }

});

/*THIS function is a hack to get the custom button to appear on the dropdown menu
Template._loginButtonsLoggedInDropdownActions.onRendered(function() {
	if(Meteor.user() && $('#YOUR_ELEMENT').length === 0 ){
		  console.log("does this work");
		  $('#login-buttons-open-change-password').before('<button class="btn btn-raised btn-info" id="YOUR_ELEMENT"> Your profile</button>');
		  $('#YOUR_ELEMENT').on('click', function() {
              console.log("Go to profile");
              var user_id = Meteor.userId(); 
              console.log(user_id);
              FlowRouter.go('/profile/' + user_id);
		  });


		}


});

*/


Template.navBar.events({
   'click #login-btn-bar-in': (event, Template) => {
      FlowRouter.go('/login');
   },

   'click #login-btn-bar-out': (event, Template) => {
      Meteor.logout();  
   },

   'click .brand-logo': (event, Template) => {
      FlowRouter.redirect('/');
   },

   'click #view-posts-btn': (event, Template) => {
      FlowRouter.go('/profile/' + Meteor.userId());
   }
});

