
Template.singleComment.helpers({
	//=> shorthand for function ()
	'printData' : (data) => {
		console.log(data);
		}

});


Template.commentVotes.events({

	'click .up-vote-comment' : (event, Template) => {
		
		var data = Template['data'];
		var id = data['_id'];
		//console.log(id);
		//console.log(data);
		var vote = data['votes'];
		//vote = vote + 1;
		console.log("HERE",vote);
		var voters = data['voters'];
		function findUser(element, index, array){
			
			//console.log(element);
			return element.user === Meteor.userId();
		}
		var idx = voters.findIndex(findUser);
		console.log(idx);
		if(idx == -1){
			vote = vote + 1;
			voters.push ( {user: Meteor.userId(), vote: 1} );
		}else{
			var element = voters[idx];
			console.log(element);
			if(voters[idx]['vote'] == -1){
				voters[idx]['vote'] = 1;
				vote = vote + 2;
			}else if(voters[idx]['vote'] == 0){
				voters[idx]['vote'] = 1;
				vote = vote + 1;
			}else if(voters[idx]['vote'] == 1){
				voters[idx]['vote'] = 0;
				vote = vote - 1;
			}
		}
		
		console.log(voters);
      /*
      when there are multiple comments, refering to a vote icon by its ID
      does not work because if you vote on multiple comments, it won't
      "know" which up/down vote button to apply CSS styling to between
      all the comments. Need to find a fix for this. If we cannot find a fix,
      we can't really make vote button effects (like color change, glow) for
      comments work properly.
       */
		//var vote_css_down = document.getElementById("vote-icon-cdow");
		//vote_css_down.style.fontWeight = "normal";
		//vote_css_down.style.color = "black";
		//vote_css_down.style.textShadow = "0px 0px 0px black";
		//var vote_css_up = document.getElementById("vote-icon-cup");
		//vote_css_up.style.fontWeight = "bold";
		//vote_css_up.style.color = "#42235A";
		//vote_css_up.style.textShadow = "0px 0px 1px #42235A";
		console.log("here",vote);
		Meteor.call('commentUpvote', id, vote, voters);
		console.log(data);
		
	},
	'click .down-vote-comment' : (event, Template) => {
		
		var data = Template['data'];
		var id = data['_id'];
		//console.log(id);
		//console.log(data);
		var vote = data['votes'];
		//vote = vote - 1;
		//console.log("HERE",vote);
		var voters = data['voters'];
		function findUser(element, index, array){
			
			console.log(element);
			return element.user === Meteor.userId();
		}
		var idx = voters.findIndex(findUser);
		console.log(idx);
		if(idx == -1){
			vote = vote - 1;
			voters.push ( {user: Meteor.userId(), vote: 1} );
		}else{
			var element = voters[idx];
			console.log(voters[idx]['vote']);
			if(voters[idx]['vote'] == -1){
				voters[idx]['vote'] = 0;
				vote = vote + 1;
			}else if(voters[idx]['vote'] == 0){
				voters[idx]['vote'] = -1;
				vote = vote - 1;
			}else if(voters[idx]['vote'] == 1){
				voters[idx]['vote'] = -1;
				vote = vote - 2;
			}
		}
		
		console.log(voters);
		
		//var vote_css_up = document.getElementById("vote-icon-cup");
		//vote_css_up.style.fontWeight = "normal";
		//vote_css_up.style.color = "black";
		//vote_css_up.style.textShadow = "0px 0px 0px black";
		//var vote_css_up = document.getElementById("vote-icon-cdow");
		//vote_css_up.style.fontWeight = "bold";
		//vote_css_up.style.color = "#42235A";
		//vote_css_up.style.textShadow = "0px 0px 1px #42235A";
		Meteor.call('commentDownvote', id, vote, voters);
		console.log(data);
		
		//Session.set('upVote',false);
		//Session.set('downVote',true);
		//Session.set('noVote',false);
		
	}
	
	

});