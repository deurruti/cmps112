Template.addPost.helpers({
    'authInProcess' : function (){
      return Meteor.loggingIn();
    },
    'canShow': function(){
       return !!Meteor.user();
    }

});


//add an event for when the 
Template.addPost.events({
   'submit form': function(event, template){

      if (! Meteor.userId()) {
        throw new Meteor.Error("not-authorized");
      }
      event.preventDefault();
      var smarkdown = Session.get('editor-markdown');
      var shtml = Session.get('editor-html');
      var sword_count = Session.get('editor-word-count'); 
      console.log(template, event);
      var listName = $('[name=newpost]').val();
      Meteor.call('addNewPost', smarkdown, shtml, sword_count, listName);
      $('[name=newpost]').val('');
    }

});

