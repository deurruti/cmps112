if(Meteor.isServer){
	Meteor.methods({
		'submitNewComment' : function(appId, smarkdown, shtml, sword_count, listName) {
			Comments.insert({
				text: listName,
				createdAt: new Date(),
				user: Meteor.userId(),      // _id of logged in user
				username: Meteor.user().username,  // username of logged in user,
				karma: 0,
				cmarkdown : smarkdown,
				html : shtml,
				word_count : sword_count,
				post: appId,
				votes: 0,
				voters: []
			});
		},
		'addNewPost' : function(smarkdown, shtml, sword_count, listName){
			Posts.collection.insert({
				text: listName,
				createdAt: new Date(),
				user: Meteor.userId(),      // _id of logged in user
				username: Meteor.user().username,  // username of logged in user,
				karma: 0,
				cmarkdown : smarkdown,
				html : shtml,
				word_count : sword_count
			});
		},
		'questionUpvote': function(post_id){
			Posts.collection.update({_id: post_id}, {$inc: {karma: 1}});
		},
		'questionDownvote': function(post_id){
			Posts.collection.update({_id: post_id}, {$inc: {karma: -1}});
		},
		'commentUpvote': function(id, vote, voters){
			Comments.update({_id: id}, {$set: {votes : vote}});
			Comments.update({_id: id}, {$set: {voters : voters}});
		},
		'commentDownvote': function(id, vote, voters){
			Comments.update({_id: id}, {$set: {votes : vote}});
			Comments.update({_id: id}, {$set: {voters : voters}});
		}
	});
}


Meteor.publish('allPosts', function(limit){
	return Posts.collection.find({}, {sort: {createdAt: -1}, limit: limit});
});

Meteor.publish('viewPost', function(post_id){
    //return the one with the id associated to it
    return [Posts.collection.find({_id : post_id}), Comments.find({post : post_id})] ;
});



Meteor.publish('viewUsersPosts', function(user_id){
	return Posts.collection.find({user : user_id});
});



