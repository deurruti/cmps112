
function checkLoggedIn(ctx, redirect){
	if (!Meteor.userId()){
       redirect('/login');
   }
}

function redirectLoggedIn(ctx, redirect){
    var id = Meteor.userId();
    if(id){
    	//later add the passing in the ID 
    	redirect('/');
    }

}

var publicRoutes = FlowRouter.group({
name : 'public'

});

//This is the home page 
publicRoutes.route('/',{
    name : 'home',
    action: function(){
    	console.log("WHAT???")
    	BlazeLayout.render('mainLayout', { top : 'navBar', content: 'postsList', bottom: 'footer'});
    },
    subscriptions: function(){
    	console.log("HERE??");
    	this.register('allPosts', Meteor.subscribeWithPagination('allPosts', 7));
    }
});

//This is the add post page 
publicRoutes.route('/addpost', {
	name: 'addPost',
	action: function(){
		BlazeLayout.render('mainLayout', { top : 'navBar',content: 'addPost', bottom: 'footer'});
	}
});

//This is the view post
publicRoutes.route('/viewpost/:post_id',{
	name : 'viewPost',
	action : function (params,queryParams){
		//console.log("HERE 1",params.post_id);
		BlazeLayout.render('mainLayout', { top : 'navBar' ,content : 'viewPost', bottom: 'footer'} );
        Session.set('visible', false);
	},
	subscriptions: function(params, queryParams){
		console.log(params.post_id);
		this.register('post', Meteor.subscribe('viewPost', params.post_id));
	} 
});


publicRoutes.route('/profile/:user_id',{
    name: 'profile',
    action : function(params, queryParams){
        BlazeLayout.render('mainLayout', { top: 'navBar', content : 'u_profile', bottom: 'footer'});
    }
});


//This is the login page
publicRoutes.route('/login', {
   name : 'login',
   action: function (params,queryParams){
      BlazeLayout.render('mainLayout', { top : 'navBar' ,content : 'viewLogin', bottom: 'footer'} );  
   }
});

