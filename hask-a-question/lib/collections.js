//Evan and Michael insert a new collection
// Comments = new Mongo.Collection("comment");

Posts =  {
	collection: new Mongo.Collection("post"),
	sorted: function (limit) {
		return this.collection.find({}, { sort: { createdAt: -1}, limit: limit});
	}
};


Comments = new Mongo.Collection("comment");
